#include <Arduino.h>

#define X_DIR 4
#define X_STP 6

#define Y_DIR1 10
#define Y_DIR2 11
#define Y_STP 8

void step (boolean dir, byte dirPin, byte stepperPin, int steps){
    digitalWrite(dirPin, dir);
    for (int i = 0; i <steps; i++) {
        digitalWrite(stepperPin, HIGH);
        delayMicroseconds (100);
        digitalWrite(stepperPin, LOW);
        delayMicroseconds (100);
    }
}

void step (boolean dir, byte dirPin1, byte dirPin2, byte stepperPin, int steps){
    digitalWrite(dirPin1, dir);
    digitalWrite(dirPin2, !dir);
    for (int i = 0; i <steps; i++) {
        digitalWrite(stepperPin, HIGH);
        delayMicroseconds (100);
        digitalWrite(stepperPin, LOW);
        delayMicroseconds (100);
    }
}

void setup(){
    pinMode (X_DIR, OUTPUT); 
    pinMode (X_STP, OUTPUT);
    pinMode (Y_DIR1, OUTPUT);
    pinMode (Y_DIR2, OUTPUT); 
    pinMode (Y_STP, OUTPUT);
}

void loop(){
    step (false, X_DIR, X_STP, 6000); // X axis motor reverse 1 ring, the 200 step is a circle. 
    delay (1000);
    step (true, X_DIR, X_STP, 6000); // X axis motor reverse 1 ring, the 200 step is a circle. 
    delay (1000);

    step (false, Y_DIR1, Y_DIR2, Y_STP, 18000); // X axis motor reverse 1 ring, the 200 step is a circle. 
    delay (1000);
    step (true, Y_DIR1, Y_DIR2, Y_STP, 18000); // X axis motor reverse 1 ring, the 200 step is a circle. 
    delay (1000);
}